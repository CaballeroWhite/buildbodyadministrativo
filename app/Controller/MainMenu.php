
<?php 

class MainMenu extends Controller{
    public function __construct()
    {
        $this->Maquinas = $this->Model("Maquina");
        $this->Usuarios = $this->Model("UsuarioCliente");
        $this->Productos = $this->Model("Producto");
        $this->Clientess = $this->Model("Cliente");
    }

    public function index(){
        $Productos = $this->Productos->GetCountProducts();
        $Maquinas = json_encode($this->Maquinas->ObtenerMaquinas());
        $Clientes = $this->Clientess->GetCountClientes();
        $Usuarios = $this->Usuarios->GetCountUser();
            $data = [
                "Maquinas" => $Maquinas,
                "CantProductos" => $Productos,
                "CantClientes" => $Clientes,
                "CantUsuarios" => $Usuarios
            ];
    
            $this->View('Main/index',$data);  
    }

    public function showData(){
        $Maquinas = json_encode($this->Maquinas->ObtenerMaquinas());

        echo $Maquinas;
    }

    public function showClientesData(){
        $AllClientes = json_encode($this->Clientess->ObtenerClientes());
        echo $AllClientes;
    }
   
}