<?php


class UsuarioCliente {
    

    public function __construct()
    {
        $this->db = new Base();
    }

    public function GetCountUser(){
        $this->db->query("SELECT Count(*) as Usuarios FROM datosusuarios");
        $Usuarios = $this->db->getOne();

        return $Usuarios;

    }


    public function ObtenerUsuarios(){
        $Usuarios = $this->db->query("SELECT * FROM datosusuarios");
        $DatosUsuarios = $this->db->getAll();

        return $DatosUsuarios;
    }


    public function CreateUser($datos){
        $this->db->query("INSERT INTO datosusuarios(Email,UserName,PassWord,ClienteId) VALUES(:Email,:UserName,:Pass,:CId)");
        $this->db->bind(":Email",$datos["Email"]);
        $this->db->bind(":UserName",$datos["UserName"]);
        $this->db->bind(":Pass",$datos["Pass"]);
        $this->db->bind(":CId",$datos["CId"]);

        if($this->db->execQuery()){
            return true;
        }else{

            return false;
        }
    }

    public function UpdateUser($id,$datos){
        $this->db->query("UPDATE datosusuarios set Email=:Email,UserName=:UserName,PassWord=:Pass WHERE ClienteId=:CId");
        $this->db->bind(":CId",$id);
        $this->db->bind(":Email",$datos["Email"]);
        $this->db->bind(":UserName",$datos["UserName"]);
        $this->db->bind(":Pass",$datos["Pass"]);
        

        if($this->db->execQuery()){
            return true;
        }else{

            return false;
        }

    }

  
    public function GetById($id){
        $this->db->query("SELECT * FROM datosusuarios WHERE id=:id");
        $this->db->bind(":id",$id);

        $Data = $this->db->GetOne();

        return $Data;

    }
     

  
}










?>